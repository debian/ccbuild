ccbuild (2.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.9 (Closes: #984007)
  * Adopt package and set new maintainer (Closes: #834636)
  * Update debian/watch for new upstream tag format
  * Update debian/rules for new upstream release
  * Bump Standards-Version to 4.5.1
  * Freshen debian/copyright
  * Add typos patch.
  * Add debian/gbp.conf

 -- tony mancill <tmancill@debian.org>  Sat, 27 Mar 2021 07:12:04 -0700

ccbuild (2.0.7+git20160227.c1179286-3) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (See #834636)
  * debian/control: migrated DH level to 13.
  * debian/patches/cross.patch:
      - Added a header.
      - Renamed to 010_cross.patch.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 05 May 2020 00:55:18 -0300

ccbuild (2.0.7+git20160227.c1179286-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

  [ Helmut Grohne ]
  * Fix FTCBFS: Use dh_auto_configure. (Closes: #950130)

  [ tony mancill ]
  * Set "Rules-Requires-Root: no" in debian/control
  * Specify debhelper compat 12 via debhelper-compat dependency
  * Bump Standards-Version to 4.5.0
  * Freshen years in debian/copyright and Homepage link
  * Update debian/watch to scan github repo

 -- tony mancill <tmancill@debian.org>  Sun, 09 Feb 2020 09:26:51 -0800

ccbuild (2.0.7+git20160227.c1179286-1) unstable; urgency=medium

  * Use an upstream version number that is actually greater than 2.0.7.

 -- tony mancill <tmancill@debian.org>  Fri, 19 Aug 2016 05:27:46 -0700

ccbuild (2.0.7~git20160227.c1179286-1) unstable; urgency=medium

  * Create new tarball from upstream git.
    (Closes: #834286)
  * Drop unneeded debian/patches.
  * Bump Standards-Version to 3.9.8.

 -- tony mancill <tmancill@debian.org>  Thu, 18 Aug 2016 22:10:46 -0700

ccbuild (2.0.7-2) unstable; urgency=medium

  * Add upstream patch to build against flex-2.6.0. (Closes: #812974)
  * Update build-dep on flex to >= 2.6.0.
  * Bump Standards-Version to 3.9.7.

 -- tony mancill <tmancill@debian.org>  Sat, 05 Mar 2016 09:54:51 -0800

ccbuild (2.0.7-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6.
  * Add tmancill@debian.org to Uploaders.
  * Adjust debian/rules for cmake-based bootstrapping of ccbuild.
  * Drop autoconf-related aspects of package build.
  * Include patches for typos in the source and manpages.

 -- tony mancill <tmancill@debian.org>  Sat, 28 Nov 2015 12:13:23 -0800

ccbuild (2.0.6-3) unstable; urgency=low

  * debian/control
    - (Build-Depends): Update from Freshmeat to Author's site.
  * debian/control and debian/rules
    - Drop explicit g++-4.8 build dependency and use default g++.
      (RC serious; Closes: #751308).
  * debian/copyright
    - Add Dimitri John Ledkov <xnox@ubuntu.com>.
  * debian/watch
    - Fix typo in comment.

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 04 Aug 2014 10:21:00 +0300

ccbuild (2.0.6-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS, flex is now fixed thus no need to fix up the sources any
    more. (Closes: #747955)

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Thu, 22 May 2014 16:20:04 +0100

ccbuild (2.0.6-2) unstable; urgency=low

  [ Jari Aalto ]
  * debian/control
    - (Build-Depends): Really update from libgnutls-dev to
      libgnutls-openssl-dev (Closes: #731620). Thanks Andreas Metzler
      <ametzler@bebt.de> for the heads up.
  * debian/patches:
    - (20): Add From header.
  * debian/copyright
    - Update year.

  [ tony mancill ]
  * debian/control and debian/rules
    - Use g++-4.8 explicitly to work around g++-4.6 on sparc.
      (Closes: #727118)

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 07 Apr 2014 07:41:28 +0300

ccbuild (2.0.6-1) unstable; urgency=low

  * New upstream release
    - libbobcat now requires --std=c++11
      (FTBFS serious; Closes: #727118).
    - Thanks Andreas Rönnquist <gusnan@gusnan.se> for helping.
  * debian/control
    - (Build-Depends): Use libgnutls-openssl-dev (Closes: #731620).
    - (Standards-Version): Update to 3.9.5.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 16 Jan 2014 10:01:34 +0200

ccbuild (2.0.5-1) unstable; urgency=low

  * New upstream release
    - Include --std=c++11 for bobcat's headers to allow for a clean
      build (Closes: #727118).
  * debian/control
    - (Standards-Version): Update to 3.9.5.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 05 Nov 2013 12:40:24 +0200

ccbuild (2.0.4-1) unstable; urgency=low

  * New upstream release
  * debian/copyright
    - Update year.

 -- Jari Aalto <jari.aalto@cante.net>  Sun, 07 Jul 2013 10:39:27 +0300

ccbuild (2.0.3+20130615+git53d9523-1) unstable; urgency=low

  * New upstream release
    - Correct Errno (FTBFS; Closes: #710618). For more information, see
      https://github.com/bneijt/ccbuild/issues/27
  * debian/control
    - (Standards-Version): Update to 3.9.4.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright:
    - Update year. Update to GPL-3+.
  * debian/rules
    - Reorder file-state-{save-restore} calls.

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 15 Jun 2013 22:24:18 +0300

ccbuild (2.0.3-1) unstable; urgency=low

  * New upstream releases (FTBFS Gcc 4.7; Closes: #667132).
  * debian/compat
    - Update to 9
  * debian/control
    - No longer a generated file. Package bobcat is now available on
      all archs.
    - (Architecture): Update to any.
    - (Build-Depends): Move to debhelper 9 and thus remove dpkg-dev.
      Require at least libbobcat-dev (>= 2.22), as it now compiles in hurd.
    - (Standards-Version): Update to 3.9.3.
  * debian/control.in:
    - Transitional file for bobcat package.
  * debian/copyright:
    - Update to Format 1.0.
  * debian/debian-save-restore.mk
    - Minor updates.
  * debian/rules:
     - Use DEB_*_MAINT_* variables.
    - (FILE_LIST_PRESERVE): Add depcomp.
    - (control): New. Generate debian/control::Architecture. Exclude
      official archs that have problems with packages like libbobcat-dev
      (hurd-i386). See RM Bug#650192.
    - (override_dh_auto_build): Fix yylex.cc with -pedantic flag.
    - Use hardened CFLAGS.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 15 May 2012 00:32:13 +0300

ccbuild (2.0.2-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright
    - (License): Add LGPL.
  * debian/patches
    - (10): Deleted. Manual page fix accepted upstream.
  * debian/rules
    - (FILE_LIST_PRESERVE): Update due to autoreconf(1).
    - (override_dh_auto_clean): Clean up files manually.

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 15 Oct 2011 10:26:27 +0300

ccbuild (2.0.1-2) unstable; urgency=low

  * debian/control
    - (Build-Depands): Require libboost-dev (>= 1.37). Add autoconf, automake.
    - (Description): Adjust line (lintian).
    - (Standards-Version): Update to 3.9.2.
  * debian/copyright
    - Update to DEP 5.
  * debian/patches
    - (20): Disable boost version check to prevent configure choking.
  * debian/rules
    - (CXXFLAGS): Change from -std=c++0x to -std=gnu++0x.
    - (CFLAGS): New
    - (FILE_LIST_PRESERVE): Remove generated files.
    - (override_dh_auto_build): Pass CXXFLAGS (FTBFS; Closes: #642699).
    - (override_dh_auto_configure): Copy fresh config.* files.
    - (override_dh_auto_clean): Clean up updated config files.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 29 Sep 2011 08:37:29 +0300

ccbuild (2.0.1-1) unstable; urgency=low

  [ Jari Aalto ]
  * New upstream release.
  * debian/clean
    - New file.
  * debian/compat
    - Update to 8.
  * debian/control
    - (Build-Depands): Update to debhelper 8.
    - (Standards-Version): Update to 3.9.1.
  * debian/copyright
    - Update download URL.
  * debian/patches
    - (10): Refresh.
  * debian/rules
    - Remove unnecessary targets and move to full dh(1).
  * debian/watch
    - Update URL.

  [ Markus Schölzel <m-schoelzel@web.de> ]
  * debian/control
    - (Build-Depands): Add libgnutls-dev, libbobcat-dev,
      pkg-config,libboost-dev.
  * debian/rules
    - (CXXFLAGS): New.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 27 Sep 2011 22:45:53 +0300

ccbuild (1.5.7-2) unstable; urgency=low

  * debian/control
    - (Build-Depends): update to debhelper 7.1.
    - (Description): Fix spelling mistake (Closes: #565735).
    - (Standards-Version): update to 3.8.4.
  * debian/copyright
    - Update upstream URL.
  * debian/debian-save-restore
    - New file.
  * debian/rules
    - (FILE_LIST_PRESERVE): New variable to Preserve original files in
      source package.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 09 Mar 2010 15:21:15 +0200

ccbuild (1.5.7-1) unstable; urgency=low

  * Initial release for Debian (Closes: #563833).
    - ACK Ubuntu package
      http://packages.ubuntu.com/lucid/ccbuild
    - Move to format: 3.0 (quilt).
  * debian/clean
    - New file.
  * debian/compat
    - Update to 7.
  * debian/control
    - (Build-Depends): update to debhelper 7.
    - (Description): Update wording.
    - (Standards-Version): Update to 3.8.3.
    - (Homepage): Point to freshmeat.
    - (Vcs-*): New fields.
  * debian/copyright
    - Update layout.
  * debian/rules
    - Update to dh(1) format.
  * debian/watch
    - Update to format 3.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 14 Jan 2010 06:35:09 +0200

ccbuild (1.5.7-0ubuntu1) jaunty; urgency=low

  * New upstream release (LP: #302775)

 -- A. Bram Neijt <bneijt@gmail.com>  Fri, 21 Nov 2008 23:32:38 +0100

ccbuild (1.5.6-0ubuntu1) hardy; urgency=low

  * New upstream release

 -- A. Bram Neijt <bneijt@gmail.com>  Sat, 01 Dec 2007 14:36:35 +0100

ccbuild (1.5.5-0ubuntu1) gutsy; urgency=low

  * Initial release

 -- A. Bram Neijt <bneijt@gmail.com>  Mon, 02 Jul 2007 17:43:30 +0200
